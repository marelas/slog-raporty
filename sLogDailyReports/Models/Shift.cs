﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace sLogDailyReports.Models
{
    public class Shift
    {
        public int Id { get; set; }
        [DisplayName("Nazwa")]
        public string ShiftName { get; set; }
        [DisplayName("Początek zmiany")]
        public int ShiftStart { get; set; }
        [DisplayName("Koniec zmiany")]
        public int ShiftEnd { get; set; }
    }
}
