﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sLogDailyReports.Models
{
    public class Group
    {
        public int Id { get; set; }
        [Display(Name="Nazwa")]
        public string Name { get; set; }
        [Display(Name="Stanowiska")]
        public IList<Station> Stations { get; set; }
        
    }
}
