﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Shifts
{
    public interface IShiftsRepository
    {
        IEnumerable<Shift> GetAll();
        void AddNew(Shift shift);
        void Update(Shift shift);
        void Delete(int id);
    }
}
