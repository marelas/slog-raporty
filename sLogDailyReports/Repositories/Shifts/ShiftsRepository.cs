﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Shifts
{
    public class ShiftsRepository : IShiftsRepository
    {
        private readonly IConfiguration _configuration;
        private ILogger<ShiftsRepository> _log;

        public ShiftsRepository(IConfiguration configuration, ILogger<ShiftsRepository> log)
        {
            _configuration = configuration;
            _log = log;
        }

        public IEnumerable<Shift> GetAll()
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "SELECT * FROM Shifts";
                var sqlCmd = new SqlCommand(cmdText, connection);
                var reader = sqlCmd.ExecuteReader();

                while (reader.Read())
                {
                    var shiftRow = new Shift
                    {
                        Id = (int)reader["ShiftId"],
                        ShiftName = (string)reader["ShiftName"],
                        ShiftStart = (int)reader["ShiftStart"],
                        ShiftEnd = (int)reader["ShiftEnd"]
                    };
                    yield return shiftRow;
                }
            }
        }

        public void AddNew(Shift shift)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "INSERT INTO Shifts (ShiftName, ShiftStart, ShiftEnd)" +
                              $"VALUES ('{shift.ShiftName}', {shift.ShiftStart}, {shift.ShiftEnd})";
                var sqlCmd = new SqlCommand(cmdText, connection);

                sqlCmd.ExecuteNonQuery();
            }
        }

        public void Update(Shift shift)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "UPDATE Shifts" +
                              $" SET ShiftName = '{shift.ShiftName}', ShiftStart = {shift.ShiftStart}, ShiftEnd = {shift.ShiftEnd}" +
                              $" WHERE ShiftId = {shift.Id}";
                var sqlCmd = new SqlCommand(cmdText, connection);

                sqlCmd.ExecuteNonQuery();
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = $"DELETE FROM Shifts WHERE ShiftId = {id}";
                var sqlCmd = new SqlCommand(cmdText, connection);

                sqlCmd.ExecuteNonQuery();
            }
        }
    }
}
