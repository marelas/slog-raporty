﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Users
{
    public class UsersRepository : IUsersRepository
    {
        private readonly IConfiguration _configuration;
        public UsersRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<User> GetUser(string userName, string password)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = $"SELECT UserName FROM Users WHERE UserName = '{userName}' AND Pass = '{HashPassword(password)}'";

                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                        return new User
                        {
                            UserName =  (string) await sqlCmd.ExecuteScalarAsync(),
                            Password = null
                        };
                }
            }
        }

        public async void SaveUser(User user)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = $"INSERT INTO Users (UserName, Pass) " +
                              $"VALUES ('{user.UserName}', '{HashPassword(user.Password)}')";

                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                    await sqlCmd.ExecuteNonQueryAsync();
                }
            }
        }

        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private static string HashPassword(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
