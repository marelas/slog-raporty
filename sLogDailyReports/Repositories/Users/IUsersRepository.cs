﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Users
{
    public interface IUsersRepository
    {
        Task<User> GetUser(string userName, string password);
        void SaveUser(User user);
    }
}
