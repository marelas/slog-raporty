﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Groups
{
    public class GroupsRepository : IGroupsRepository
    {
        private readonly IConfiguration _configuration;
        public GroupsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IEnumerable<Group> GetAll()
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "SELECT GroupId, GroupName FROM Groups";
                var sqlCmd = new SqlCommand(cmdText, connection);

                var reader = sqlCmd.ExecuteReader();

                while (reader.Read())
                {
                    yield return new Group {Id = (int) reader["GroupId"], Name = (string) reader["GroupName"]};
                }
            }
        }

        public Group GetById(int id)
        {
            Group result = null;
            var stations = new List<Station>();

            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = $"SELECT GroupId, GroupName FROM Groups WHERE GroupId = {id}";
                var sqlCmd = new SqlCommand(cmdText, connection);

                using (var reader = sqlCmd.ExecuteReader())
                {
                    reader.Read();
                    result = new Group { Id = (int)reader["GroupId"], Name = (string)reader["GroupName"] };

                    cmdText = $"SELECT ID, Stanowisko, Opis FROM Stanowiska WHERE IdGroup = {id}";
                    sqlCmd = new SqlCommand(cmdText, connection);
                }

                using (var reader = sqlCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        stations.Add(new Station
                        {
                            Id = (int)reader["ID"],
                            Name = (string)reader["Stanowisko"],
                            Description = reader["Opis"] == DBNull.Value ? "" : (string)reader["Opis"]
                        });
                    }
                }
            }

            result.Stations = stations;

            return result;
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = $"DELETE FROM Groups WHERE GroupId = {id}";
                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }

                cmdText = $"UPDATE stanowiska " +
                          $"SET IdGroup = NULL " +
                          $"WHERE IdGroup = {id}";
                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }
            }
        }

        public void Create(Group group)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "INSERT INTO Groups " +
                              "(GroupName) " +
                              "OUTPUT inserted.GroupId " +
                              $"VALUES ('{group.Name}')";
                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                    using (var reader = sqlCmd.ExecuteReader())
                    {
                        reader.Read();
                        group.Id = (int)reader[0];
                    }
                }
            }
        }

        public void Edit(Group group)
        {
            throw new NotImplementedException();
        }

        public void AddStations(int group, int[] stations)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "UPDATE Stanowiska " +
                             $"SET IdGroup = {group} " +
                             $"WHERE ID in ({string.Join(',', stations)})";

                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }
            }
        }

        public void RemoveStations(int group, int[] stations)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "UPDATE Stanowiska " +
                              $"SET IdGroup = NULL " +
                              $"WHERE ID in ({string.Join(',', stations)})";

                using (var sqlCmd = new SqlCommand(cmdText, connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }
            }
        }
    }
}
