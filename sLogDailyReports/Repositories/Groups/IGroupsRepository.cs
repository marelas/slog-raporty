﻿using sLogDailyReports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sLogDailyReports.Repositories.Groups
{
    public interface IGroupsRepository
    {
        IEnumerable<Group> GetAll();
        void Delete(int id);
        void Create(Group group);
        void Edit(Group group);
        Group GetById(int id);

        void AddStations(int group, int[] stations);
        void RemoveStations(int group, int[] stations);
    }
}
