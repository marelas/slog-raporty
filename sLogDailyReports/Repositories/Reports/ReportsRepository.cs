﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace sLogDailyReports.Repositories.Reports
{
    public class ReportsRepository : IReportsRepository
    {
        private readonly IConfiguration _configuration;
        public ReportsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IEnumerable<(int Hour, int Amount)> GetReport(int groupId, int shiftId, DateTime date)
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var dateFormatted = date.ToString("yyyy-MM-dd");

                var cmdText =
                    $"EXEC ReportReleasesFromPackStation @ReportDate = '{dateFormatted}', @GroupId = {groupId}, @ShiftID = {shiftId}";
                using (var cmd = new SqlCommand(cmdText, connection))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return ((int) reader["godzina"], (int) reader["ilosc"]);
                        }
                    }
                }
            }
        }
    }
}
