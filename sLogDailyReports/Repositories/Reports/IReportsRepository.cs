﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sLogDailyReports.Repositories.Reports
{
    public interface IReportsRepository
    {
        IEnumerable<(int Hour, int Amount)> GetReport(int groupId, int shiftId, DateTime date);
    }
}
