﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Stations
{
    public interface IStationsRepository
    {
        IEnumerable<Station> GetAllUnassigned();
    }
}
