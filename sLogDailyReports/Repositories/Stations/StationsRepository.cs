﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using sLogDailyReports.Models;

namespace sLogDailyReports.Repositories.Stations
{
    public class StationsRepository : IStationsRepository
    {
        private readonly IConfiguration _configuration;
        public StationsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IEnumerable<Station> GetAllUnassigned()
        {
            using (var connection = new SqlConnection(_configuration["sLogDbConnectionString"]))
            {
                connection.Open();

                var cmdText = "SELECT ID, Stanowisko, Opis FROM Stanowiska WHERE IdGroup IS NULL";
                var sqlCmd = new SqlCommand(cmdText, connection);

                using (var reader = sqlCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new Station
                        {
                            Id = (int)reader["ID"],
                            Name = (string)reader["Stanowisko"],
                            Description = reader["Opis"] == DBNull.Value ? "" : (string)reader["Opis"]
                        };
                    }
                }
            }
        }
    }
}
