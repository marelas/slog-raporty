﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using sLogDailyReports.Models;
using sLogDailyReports.Repositories.Users;

namespace sLogDailyReports.Pages.Account
{
    public class RegisterModel : PageModel
    {
        [BindProperty]
        public User InputUser { get; set; }

        private readonly ILogger<RegisterModel> _logger;
        private readonly IUsersRepository _usersRepository;

        public RegisterModel(ILogger<RegisterModel> logger, IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
            _logger = logger;
        }
        public void OnGet()
        {

        }

        public IActionResult OnPost()
        {
            _usersRepository.SaveUser(InputUser);

            return RedirectToPage("/Account/Login");
        }
    }
}