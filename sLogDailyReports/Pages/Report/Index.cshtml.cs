﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sLogDailyReports.Models;
using sLogDailyReports.Repositories.Groups;
using sLogDailyReports.Repositories.Shifts;
using sLogDailyReports.Repositories.Stations;

namespace sLogDailyReports.Pages.Report
{
    [Authorize]
    public class IndexModel : PageModel
    {
        public IList<Group> Groups { get; set; }
        public IList<Shift> Shifts { get; set; }

        public Shift SelectedShift { get; set; }
        public Group SelectedGroup { get; set; }
        public DateTime SelectedDate { get; set; }

        private readonly IGroupsRepository _groupsRepository;
        private readonly IShiftsRepository _shiftsRepository;

        public IndexModel(IGroupsRepository groupsRepository, IShiftsRepository shiftsRepository)
        {
            _groupsRepository = groupsRepository;
            _shiftsRepository = shiftsRepository;
        }

        public void OnGet()
        {
            Groups = _groupsRepository.GetAll().ToList();
            Shifts = _shiftsRepository.GetAll().ToList();
            SelectedDate = DateTime.Today;
        }

        public void OnGetShowReport()
        {
            var r = Request.Form;
        }
    }
}