﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sLogDailyReports.Models;
using sLogDailyReports.Repositories.Groups;
using sLogDailyReports.Repositories.Reports;
using sLogDailyReports.Repositories.Shifts;

namespace sLogDailyReports.Pages.Report
{
    [Authorize]
    public class ShowModel : PageModel
    {
        public IList<(int Hour, int Amount)> Report;
        public Shift Shift { get; set; }
        public Group Group { get; set; }

        private readonly IReportsRepository _reportsRepository;
        private readonly IShiftsRepository _shiftsRepository;
        private readonly IGroupsRepository _groupsRepository;
        public ShowModel(IReportsRepository reportsRepository, IShiftsRepository shiftsRepository, IGroupsRepository groupsRepository)
        {
            _reportsRepository = reportsRepository;
            _shiftsRepository = shiftsRepository;
            _groupsRepository = groupsRepository;
        }

        public void OnGet(int selectedGroup, int selectedShift, DateTime selectedDate)
        {
            Report = new List<(int Hour, int Amount)>();
            var packages =_reportsRepository.GetReport(selectedGroup, selectedShift, selectedDate).ToList();

            Shift = _shiftsRepository.GetAll().Single(s => s.Id == selectedShift);
            Group = _groupsRepository.GetById(selectedGroup);

            for (var i = 0; i < Math.Abs(Shift.ShiftEnd - Shift.ShiftStart); i++)
            {
                int amount = 0;
                int hour = (i + Shift.ShiftStart) % 24;
                if (packages.Exists(p => p.Hour == hour))
                {
                    amount = packages.Single(p => p.Hour == hour).Amount;
                }
                Report.Add((hour, amount));
            }
        }
    }
}