﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sLogDailyReports.Models;
using sLogDailyReports.Repositories.Groups;
using sLogDailyReports.Repositories.Stations;

namespace sLogDailyReports.Pages.Groups
{
    [Authorize]
    public class EditModel : PageModel
    {
        public Group Group { get; set; }
        public IList<Station> AvailableStations { get; set; }

        private readonly IGroupsRepository _groupsRepository;
        private readonly IStationsRepository _stationsRepository;

        public EditModel(IGroupsRepository groupsRepository, IStationsRepository stationsRepository)
        {
            _groupsRepository = groupsRepository;
            _stationsRepository = stationsRepository;
        }

        public void OnGet(int id)
        {
            Group = _groupsRepository.GetById(id);
            AvailableStations = _stationsRepository.GetAllUnassigned().ToList();
        }

        public IActionResult OnPostSaveGroup()
        {
            var stationsIds = Request.Form["StationId"].ToArray();
            var groupId = int.Parse(Request.Path.ToString().Split('/').Last());

            _groupsRepository.AddStations(groupId, Array.ConvertAll(stationsIds, id => int.Parse(id)));

            return RedirectToPage("/Groups/Edit", new { id = groupId });
        }

        public IActionResult OnPostRemoveStations()
        {
            var groupId = int.Parse(Request.Path.ToString().Split('/').Last());
            var stationsIds = Request.Form["StationId"].ToArray();

            _groupsRepository.RemoveStations(groupId, Array.ConvertAll(stationsIds, id => int.Parse(id)));

            return RedirectToPage("/Groups/Edit", new { id = groupId });
        }
    }
}