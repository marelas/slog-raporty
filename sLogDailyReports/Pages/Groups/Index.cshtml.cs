﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sLogDailyReports.Models;
using sLogDailyReports.Repositories.Groups;

namespace sLogDailyReports.Pages.Groups
{
    [Authorize]
    public class IndexModel : PageModel
    {
        public List<Group> Groups { get; set; }

        [BindProperty]
        public Group NewGroup { get; set; }

        private readonly IGroupsRepository _groupsRepository;

        public IndexModel(IGroupsRepository groupsRepository)
        {
            _groupsRepository = groupsRepository;
        }

        public void OnGet()
        {
            Groups = _groupsRepository.GetAll().ToList();
            NewGroup = new Group();
        }

        public IActionResult OnPostSaveGroup()
        {
            _groupsRepository.Create(NewGroup);
            return RedirectToPage("/Groups/Edit", new { id = NewGroup.Id });
        }

        public IActionResult OnPostDeleteGroup()
        {
            var r = Request.Form;
            _groupsRepository.Delete(int.Parse(Request.Form["Id"]));

            return new JsonResult(new { success = true });
        }
    }
}