﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using sLogDailyReports.Models;
using sLogDailyReports.Repositories.Shifts;

namespace sLogDailyReports.Pages
{
    [Authorize]
    public class ShiftsModel : PageModel
    {
        [BindProperty]
        public List<Shift> Shifts { get; set; }

        [BindProperty]
        public Shift NewShift { get; set; }

        public List<SelectListItem> Hours { get; set; }

        public bool EditMode = false;

        private readonly IShiftsRepository _shiftsRepository;

        public ShiftsModel(IShiftsRepository shiftsRepository)
        {
            _shiftsRepository = shiftsRepository;
        }

        public void OnGet()
        {
            Hours = new List<SelectListItem>();
            for (int i = 0; i < 24; i++)
            {
                Hours.Add(new SelectListItem(i + ":00", i.ToString()));
            }
            Shifts = _shiftsRepository.GetAll().ToList();
        }

        public IActionResult OnPostDelete(int id)
        {
            _shiftsRepository.Delete(id);

            return RedirectToPage("Shifts");
        }

        public IActionResult OnPostSaveShift(int id, List<Shift> shifts)
        {
            _shiftsRepository.Update(shifts.Single(s => s.Id == id));
            return RedirectToPage("Shifts");
        }

        public IActionResult OnPostSaveNewShift()
        {
            _shiftsRepository.AddNew(NewShift);
            return RedirectToPage("Shifts");
        }
    }
}